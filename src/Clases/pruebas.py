from datetime import datetime


class Persona(object):

    def __init__(self, nombre, dni, fecha_nacimiento):
        self.nombre = nombre
        self.dni = dni
        self.fecha_nacimiento = fecha_nacimiento

    @property
    def edad(self):
        _now = datetime.today()
        _nac_obj = datetime.strptime(
            self.fecha_nacimiento,
            '%d/%m/%Y'
        )
        return _now.year - _nac_obj.year - (
            (_now.month, _now.day) < (_nac_obj.month, _nac_obj.day)
        )
