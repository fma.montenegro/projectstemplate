def make_fibonacci(limit, verbose=False) -> list:
    """
        Genera la serie de fibonacci hasta n
    """
    a = 0
    b = 1
    result = []
    while a < limit:
        if verbose:
            print(a, end=', ')
        result.append(a)
        a, b = b, a + b
    if verbose:
        print()
    return result

def muchos_items(separador, *args):
    print(separador.join(args))

def muchos_items_palabras(**kwargs):
    for key in kwargs.keys():
        print('{0}: {1}'.format(key, kwargs[key]))

def es_mayor_100(numero) -> bool:
    return numero > 100