import requests, json

def create_account(input_user, input_pass):
    url = 'http://54.237.198.158:5000/create_user'
    payload = {
        'user': input_user,
        'password': input_pass
    }
    response = requests.get(url, params=payload)
    # import ipdb; ipdb.set_trace()
    json_response = response.json()
    return json_response

if __name__ == "__main__":
    u = input("Usuario: ")
    p = input("Password: ")
    response = create_account(u,p)
    if 'status' in response and response['status'] == 'OK':
        print('Usuario creado correctamente.')
    else:
        print('Un error ocurrio al crear el usuario, intente nuevamente.')