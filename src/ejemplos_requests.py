import requests, json

# #url_post = 'https://httpbin.org/post'
# #url_get = 'https://api.github.com/events'
# url_curso = 'http://127.0.0.1:5000/api/v1/resultado/?alumno=Montenegro'
# #post_data = {'key':'value'}

# def make_requests(method, url, params=None):
#     method = method.upper()
#     if method == 'GET':
#         r = requests.get(url)
#     elif method == 'POST':
#         r = requests.post(url, data = params)
#     content = r.content
#     return content


# if '__main__' == __name__:
#     #make_requests('post', url_post, post_data)
#     print(make_requests('get', url_curso))

url_curso = 'http://127.0.0.1:5000/api/v1/resultado/?alumno=Montenegro'

def get(url):
    r = requests.get(url)
    content = r.content
    return content

if '__main__' == __name__:
    print(get(url_curso))